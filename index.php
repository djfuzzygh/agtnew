<!DOCTYPE html>
<html lang="fr">
    <head>

        <!-- Basic Page Needs
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <meta charset="utf-8">
        <title>AGT: Incroyable Talent</title>
        <meta name="description" content="L’Afrique A Un Incroyable Talent  a pour ambition de découvrir et mettre en valeur les meilleurs talents d’Afrique Francophone sous la supervision du jury composé de membres éminents de la scène culturelle Africaine : Angélique Kidjo, Fally Ipupa et Claudia Tagbo.
            Soutenu par son Partenaire Officiel Nescafé, ce programme séduit tous les téléspectateurs désireux de passer une belle soirée devant leur écran de télévision.
            Le Prix Nescafé donne l’opportunité au public de prendre la décision finale et de choisir leur candidat préféré en votant en ligne.">
        <meta name="keywords" content="">
        <meta property="og:title" content="">
        <meta property="og:description" content="">
        <meta property="og:url" content="">
        <meta property="og:site_name" content="">
        <meta property="og:type" content="website">
        <meta name="twitter:url" content="">
        <meta name="twitter:title" content="">
        <meta name="author" content="">

        <!-- Mobile Specific Metas
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- FONT
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

        <!-- CSS
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/skeleton.css">
        <link rel="stylesheet" href="css/custom.css">
        <link rel="stylesheet" href="https://unpkg.com/flickity@2.0/dist/flickity.css" media="screen">

        <!-- Scripts
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://unpkg.com/flickity@2.0/dist/flickity.pkgd.min.js"></script>

        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window,document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '302643959930499');
            fbq('track', 'PageView');
        </script>
        <noscript>
            <img height="1" width="1"
            src="https://www.facebook.com/tr?id=302643959930499&ev=PageView&noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->

        <!-- Favicon
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <link rel="icon" type="image/png" href="https://prod1.nescafe.com/cwa_en/public/img/favicon/favicon.ico">

    </head>
    <body>

        <!-- Primary Page Layout
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <div class="section header">
            <div class="row brand">
                <a href="/"><img src="images/logo_03.png"/ alt="logo"></a>
            </div>
            <nav class="row navbar">
                <div class="container">
                  <a class="navbar-link" href="">#Accueil</a>
                  <a class="navbar-link" href="finalistes.php">#Finalistes</a>
                  <a class="navbar-link" href="reglement.php">#Réglement</a>
                  <!--a class="navbar-link" href="#">#Profils</a>
                  <a class="navbar-link" href="#">#Gagnant</a-->
                </div>
            </nav>
        </div>
        <!-- <div class="backgroundleft">
          <div class="backgroundright"> -->
        <div class="section banner">
            <a href="/finalistes.php"><img src="images/home-banner.png"></a>
              </div>


        <div class="section roll">
            <div class="container">
                <!-- .roll of finalist -->
                <section>
                    <div class="carousel row" data-flickity='{ "imagesLoaded": true, "autoPlay": true, "wrapAround": false, "prevNextButtons": false, "contain": true, "pageDots": false}'>
                <!-- <div class="row finalist-roll"> -->
                <div class="three columns finalist-thumbnail">
                    <figure>
                        <img src="images/profile/LUMIERES.png" alt="Lumieres D'Afrique">
                        <span class="three columns nametag">Lumieres D'Afrique</span>
                    </figure>
                    <a href="profils/lumieres.php" class="view-profile-button">voir profil</a>
                </div>
                <div class="three columns finalist-thumbnail">
                    <figure>
                        <img src="images/profile/NSONA.png" alt="Nsona Wemba">
                        <span class="three columns nametag">Nsona Wemba</span>
                    </figure>
                    <a href="profils/nsona.php" class="view-profile-button">voir profil</a>
                </div>
                <div class="three columns finalist-thumbnail">
                    <figure>
                        <img src="images/profile/BAKIN.png" alt="Bakin Gado">
                        <span class="three columns nametag">Bak'in Gado</span>
                    </figure>
                    <a href="profils/bakin.php" class="view-profile-button">voir profil</a>
                </div>
                <div class="three columns finalist-thumbnail">
                    <figure>
                        <img src="images/profile/JULIENNE.png" alt="Julienne">
                        <span class="three columns nametag">Julienne</span>
                    </figure>
                    <a href="profils/julienne.php" class="view-profile-button">voir profil</a>
                </div>
                <div class="three columns finalist-thumbnail">
                    <figure>
                        <img src="images/profile/SUADU.png" alt="Suadu">
                        <span class="three columns nametag">Suadu</span>
                    </figure>
                    <a href="profils/suadu.php" class="view-profile-button">voir profil</a>
                </div>
                <div class="three columns finalist-thumbnail">
                    <figure>
                        <img src="images/profile/CLEMENTINE.png" alt="Clementine">
                        <span class="three columns nametag">Clementine</span>
                    </figure>
                    <a href="profils/clementine.php" class="view-profile-button">voir profil</a>
                </div>
                <div class="three columns finalist-thumbnail">
                    <figure>
                        <img src="images/profile/DJAROULE.png" alt="Djaroule">
                        <span class="three columns nametag">Djaroule</span>
                    </figure>
                    <a href="profils/djaroule.php" class="view-profile-button">voir profil</a>
                </div>
                <div class="three columns finalist-thumbnail">
                    <figure>
                        <img src="images/profile/JUNIOR.png" alt="Junior">
                        <span class="three columns nametag">Junior</span>
                    </figure>
                    <a href="profils/junior.php" class="view-profile-button">voir profil</a>
                </div>
                <div class="three columns finalist-thumbnail">
                    <figure>
                        <img src="images/profile/BONE.jpg" alt="Bone Breakers">
                        <span class="three columns nametag">Bone Breakers</span>
                    </figure>
                    <a href="profils/bone-breakers.php" class="view-profile-button">voir profil</a>
                </div>
                <div class="three columns finalist-thumbnail">
                    <figure>
                        <img src="images/profile/VISION.png" alt="Vision Royale">
                        <span class="three columns nametag">Vision Royale</span>
                    </figure>
                    <a href="profils/vision.php" class="view-profile-button">voir profil</a>
                </div>
                <div class="three columns finalist-thumbnail">
                    <figure>
                        <img src="images/profile/THIBAULT.png" alt="Thibault">
                        <span class="three columns nametag">Thibault</span>
                    </figure>
                    <a href="profils/thibault.php" class="view-profile-button">voir profil</a>
                </div>
                <div class="three columns finalist-thumbnail">
                    <figure>
                        <img src="images/profile/AANINKA.png" alt="Aaninka">
                        <span class=" three columns nametag">Aaninka</span>
                    </figure>
                    <a href="profils/aaninka.php" class="view-profile-button">voir profil</a>
                </div>

                <!-- </div> -->
            </div>
        </div>
      </div>
  </section>
        <div class="section video-highlight">
            <div class="container">
                <div class="twelve columns video-box">
                    <i class="column video-reel"></i>
                    <h3 class="nine columns">Appréciez les meilleurs moments</h3>
                    <iframe width="853" height="480" src="https://www.youtube.com/embed/jVsXxipzMHw?rel=0" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
          </div>
        <!-- </div>
      </div> -->
    </div>
  </div>

        <!-- End Document
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    </body>
</html>

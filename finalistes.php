<!DOCTYPE html>
<html lang="fr">
    <head>

        <!-- Basic Page Needs
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <meta charset="utf-8">
        <title>AGT: Incroyable Talent</title>
        <meta name="description" content="L’Afrique A Un Incroyable Talent  a pour ambition de découvrir et mettre en valeur les meilleurs talents d’Afrique Francophone sous la supervision du jury composé de membres éminents de la scène culturelle Africaine : Angélique Kidjo, Fally Ipupa et Claudia Tagbo.
            Soutenu par son Partenaire Officiel Nescafé, ce programme séduit tous les téléspectateurs désireux de passer une belle soirée devant leur écran de télévision.
            Le Prix Nescafé donne l’opportunité au public de prendre la décision finale et de choisir leur candidat préféré en votant en ligne.">
        <meta name="author" content="">

        <!-- Mobile Specific Metas
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- FONT
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

        <!-- CSS
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/skeleton.css">
        <link rel="stylesheet" href="css/custom.css">

        <!-- Scripts
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window,document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '302643959930499');
            fbq('track', 'PageView');
        </script>
        <noscript>
            <img height="1" width="1"
            src="https://www.facebook.com/tr?id=302643959930499&ev=PageView&noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->

        <!-- Favicon
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <link rel="icon" type="image/png" href="https://prod1.nescafe.com/cwa_en/public/img/favicon/favicon.ico">

    </head>
    <body>

        <!-- Primary Page Layout
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <div class="section header">
            <div class="row brand">
                <a href="/"><img src="images/logo_03.png"/ alt="logo"></a>
            </div>
            <nav class="row navbar">
                <div class="container">
                  <a class="navbar-link" href="index.php">#Accueil</a>
                  <a class="navbar-link" href="">#Finalistes</a>
                  <a class="navbar-link" href="reglement.php">#Réglement</a>
                  <!--a class="navbar-link" href="#">#Profils</a>
                  <a class="navbar-link" href="#">#Gagnant</a-->
                </div>
            </nav>
        </div>
        <!-- <div class="backgroundleft">
          <div class="backgroundright"> -->
        <div class="section finalists">
          <!-- <div class="backgroundleft">
            <div class="backgroundright"> -->
            <div class="container">
                <div class="twelve columns finalist-grid">
                    <h3 class="title">Nos Incroyables Talents</h3>
                    <p>Après avoir sillonné et auditionné plus de 5000 talents en Côte d’Ivoire,
                        au Burkina Faso, au Mali, au Sénégal, ainsi qu’au Cameroun, maintenant
                        démarre la prochaine étape de la compétition « L’Afrique A Un Incroyable
                        Talent » : vibrez et soutenez le talent qui vous inspire le plus parmi les
                        12 finalistes.<p>
                    <div class="row grid">
                        <div class="four columns finalist-thumbnail">
                            <figure>
                                <img src="images/profile/LUMIERES.png" alt="Lumieres D'Afrique">
                                <span class="three columns nametag">Acrobates - Lumieres D'Afrique</span>
                            </figure>
                            <a href="profils/lumieres.php" class="view-profile-button">voir profil</a>
                        </div>
                        <div class="four columns finalist-thumbnail">
                            <figure>
                                <img src="images/profile/NSONA.png" alt="Nsona Wemba">
                                <span class="three columns nametag">Chanteuse - Nsona Wemba</span>
                            </figure>
                            <a href="profils/nsona.php" class="view-profile-button">voir profil</a>
                        </div>
                        <div class="four columns finalist-thumbnail">
                            <figure>
                                <img src="images/profile/BAKIN.png" alt="Bakin Gado">
                                <span class="three columns nametag">Conteurs Musicaux - Bak'in Gado</span>
                            </figure>
                            <a href="profils/bakin.php" class="view-profile-button">voir profil</a>
                        </div>
                    </div>
                    <div class="row grid">
                        <div class="four columns finalist-thumbnail">
                            <figure>
                                <img src="images/profile/JULIENNE.png" alt="Julienne">
                                <span class="three columns nametag">Chanteuse - Julienne</span>
                            </figure>
                            <a href="profils/julienne.php" class="view-profile-button">voir profil</a>
                        </div>
                        <div class="four columns finalist-thumbnail">
                            <figure>
                                <img src="images/profile/SUADU.png" alt="Suadu">
                                <span class="three columns nametag">Chanteuse - Suadu</span>
                            </figure>
                            <a href="profils/suadu.php" class="view-profile-button">voir profil</a>
                        </div>
                        <div class="four columns finalist-thumbnail">
                            <figure>
                                <img src="images/profile/CLEMENTINE.png" alt="Clementine">
                                <span class="three columns nametag">Danseuse - Clementine</span>
                            </figure>
                            <a href="profils/clementine.php" class="view-profile-button">voir profil</a>
                        </div>
                    </div>
                    <div class="row grid">
                        <div class="four columns finalist-thumbnail">
                            <figure>
                                <img src="images/profile/DJAROULE.png" alt="Djaroule">
                                <span class="three columns nametag">Contorsionniste - Djaroule</span>
                            </figure>
                            <a href="profils/djaroule.php" class="view-profile-button">voir profil</a>
                        </div>
                        <div class="four columns finalist-thumbnail">
                            <figure>
                                <img src="images/profile/JUNIOR.png" alt="Junior">
                                <span class="three columns nametag">Acrobate - Junior</span>
                            </figure>
                            <a href="profils/junior.php" class="view-profile-button">voir profil</a>
                        </div>
                        <div class="four columns finalist-thumbnail">
                            <figure>
                                <img src="images/profile/BONE.jpg" alt="Bone Breakers">
                                <span class="three columns nametag">Contorsionniste - Bone Breakers</span>
                            </figure>
                            <a href="profils/bone-breakers.php" class="view-profile-button">voir profil</a>
                        </div>
                    </div>
                    <div class="row grid">
                        <div class="four columns finalist-thumbnail">
                            <figure>
                                <img src="images/profile/VISION.png" alt="Vision Royale">
                                <span class="three columns nametag">Danseurs - Vision Royale</span>
                            </figure>
                            <a href="profils/vision.php" class="view-profile-button">voir profil</a>
                        </div>
                        <div class="four columns finalist-thumbnail">
                            <figure>
                                <img src="images/profile/THIBAULT.png" alt="Thibault">
                                <span class="three columns nametag">Magicien - Thibault</span>
                            </figure>
                            <a href="profils/thibault.php" class="view-profile-button">voir profil</a>
                        </div>
                        <div class="four columns finalist-thumbnail">
                            <figure>
                                <img src="images/profile/AANINKA.png" alt="Aaninka">
                                <span class=" three columns nametag">Danseurs - Aaninka</span>
                            </figure>
                            <a href="profils/aaninka.php" class="view-profile-button">voir profil</a>
                        </div>
                    </div>
                    <div class="twelve columns foot-notes">
                      <div class="seven columns nescafe-logo">
                          <img src="images/logo-white.png" alt="" class="">
                          <ul class="social">
                              <li><a href="https://www.facebook.com/Nescafe.GH/" target="_blank"><img src="images/facebook.svg" alt=""></a></li>
                              <li><a href="https://twitter.com/NescafeAfrica" target="_blank"><img src="images/twitter.svg" alt=""></a></li>
                              <li><a href="https://www.youtube.com/user/NESCAFEAfrica" target="_blank"><img src="images/youtube.svg" alt=""></a></li>
                              <li><a href="https://www.instagram.com/nescafeafrica/" target="_blank"><img src="images/instagram.svg" alt=""></a></li>
                          </ul>
                      </div>
                        <div class="four columns agt-logo">
                            <img src="images/talent-logo.png" alt="" class="">
                            <ul class="row social agt-si">
                                <li><a href="https://www.facebook.com/afriqueaunincroyabletalent/" target="_blank"><img src="images/facebook.svg" alt=""></a></li>
                                <li><a href="https://www.youtube.com/channel/UC0KCCM5Y1j74Lkkxp0VsHHw" target="_blank"><img src="images/youtube.svg" alt=""></a></li>
                                <li><a href="https://www.instagram.com/incroyable_talent_afrique/" target="_blank"><img src="images/instagram.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>

        <!-- End Document
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    </body>
</html>

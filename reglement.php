<!DOCTYPE html>
<html lang="fr">
    <head>

        <!-- Basic Page Needs
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <meta charset="utf-8">
        <title>AGT: Incroyable Talent - Réglement</title>
        <meta name="description" content="L’Afrique A Un Incroyable Talent  a pour ambition de découvrir et mettre en valeur les meilleurs talents d’Afrique Francophone sous la supervision du jury composé de membres éminents de la scène culturelle Africaine : Angélique Kidjo, Fally Ipupa et Claudia Tagbo.
            Soutenu par son Partenaire Officiel Nescafé, ce programme séduit tous les téléspectateurs désireux de passer une belle soirée devant leur écran de télévision.
            Le Prix Nescafé donne l’opportunité au public de prendre la décision finale et de choisir leur candidat préféré en votant en ligne.">
        <meta name="author" content="">

        <!-- Mobile Specific Metas
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- FONT
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

        <!-- CSS
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/skeleton.css">
        <link rel="stylesheet" href="css/custom.css">

        <!-- Scripts
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window,document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '302643959930499');
            fbq('track', 'PageView');
        </script>
        <noscript>
            <img height="1" width="1"
            src="https://www.facebook.com/tr?id=302643959930499&ev=PageView&noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->

        <!-- Favicon
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <link rel="icon" type="image/png" href="https://prod1.nescafe.com/cwa_en/public/img/favicon/favicon.ico">

    </head>
    <body>

        <!-- Primary Page Layout
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <div class="section header">
            <div class="row brand">
                <a href="/"><img src="images/logo_03.png"/ alt="logo"></a>
            </div>
            <nav class="row navbar">
                <div class="container">
                  <a class="navbar-link" href="index.php">#Accueil</a>
                  <a class="navbar-link" href="finalistes.php">#Finalistes</a>
                  <a class="navbar-link" href="">#Réglement</a>
                  <!--a class="navbar-link" href="#">#Profils</a>
                  <a class="navbar-link" href="#">#Gagnant</a-->
                </div>
            </nav>
        </div>
        <div class="section finalists">
          <!-- <div class="backgroundleft">
            <div class="backgroundright"> -->
            <div class="container">
                <div class="twelve columns rules">
                    <h3 class="title">REGLES ET CONDITIONS DU PRIX NESCAFÉ</h3>
                    <ol>
                      <li>NESCAFE partenaire officielle de l’émission l’Afrique a un incroyable a soutenu les incroyables talents à chaque étape. Nous avons été touchés et nous ne pouvons-nous résoudre à dire au revoir aux 12 finalistes.
Ces talents ont inspiré toute l’Afrique et  à notre tour, nous voulons  donner une autre opportunité d’accomplir leur rêves en organisant une compétition ou les  téléspectateurs voteront en ligne pour leur candidat préféré, celui qui aura le plus de vote obtiendra l’honneur d’être sacré et de recevoir le Prix Nescafé et de continuer à inspirer la jeunesse Africaine!</li>
                      <li>Le gagnant prix NESCAFÉ de l’Afrique A Un Incroyable Talent remportera la somme de 3 million de FCFA.</li>
                        <li>Uniquement les résidents de la Cote d’Ivoire, Cameroun, Tchad,  Gabon, République Centre Africaine, Burkina-Faso, Guinée, Togo, Benin, Mali, Niger,  Sénégal, Cape Vert, Guinée Bissau, Sao Tome & Principe, Guinée Équatorial, Mauritanie peuvent prendre part au vote pour le Gagnant prix NESCAFÉ de l’Afrique A Un Incroyable Talent qui se fera en ligne sur le site (lien).
                        <li>La période de vote s’étend du 20/02/2017 au 03/03/2017.  NESTLE/NESCAFE se réserve le droit d’étendre la période de vote. </li>
                        <li>Chaque personne n’est autorisée à voter qu’une seule fois par jour pour ses candidats préférés en utilisant son Facebook pour authentifier son vote.</li>
                        <li>Aucun membre de l’équipe NESTLÉ/NESCAFÉ ou de ses agences ou personnes directement liées au projet (quelle qu’en soit la forme) n’est autorisé à voter.</li>
                        <li>NESTLÉ/NESCAFÉ se réserve le droit de modifier ou d’annuler cette compétition du gagnant prix NESCAFÉ de l’Afrique A Un Incroyable Talent à tout moment et sans notification en cas de force majeur.</li>
                        <li>NESTLÉ/NESCAFÉ et/ou ses agences n’accepteront aucune responsabilité concernant des problèmes techniques ou de dysfonctionnements ou tout autre problème lié aux réseaux ou à la ligne téléphonique, au serveur, au fournisseur d’accès ou autre pouvant entraîner des pertes de votes n’ayant pu être téléchargés, identifiés ou soumis correctement via le site internet et pour toute autre raison hors de son contrôle comprenant les cas de force majeure.</li>
                        <li>NESTLÉ/NESCAFÉ et/ou ses agences ne seront responsables ou assujettis à l’échec d’aucun candidat quelle qu’en soit la raison.</li>
                        <li>Hormis une responsabilité ne pouvant être exclue par la loi, NESTLÉ/NESCAFÉ (comprenant ses employés, ses agents ses préposés) écarte toute responsabilité pour tout préjudice personnel ou perte ou désagrément (comprenant la perte d’opportunité) que ce soit direct ou indirect, spécial ou consécutif, survenant de quelque manière que ce soit lors de la promotion/compétition du prix NESCAFÉ comprenant et sans limitation à : (i) toute difficulté technique, (ii) dysfonctionnement de l’équipement, (iii) l’interférence d’un tiers, (iv) des raisons indépendantes de sa volonté ou (v) toute autre responsabilité encourue par un participant.</li>
                        <li>Tout participant au jeu reconnaît avoir pris connaissance des présentes conditions et les accepter en toutes ses dispositions.</li>
                    </ol>
                </div>
        </div>
</div>
</div>
        <!-- End Document
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    </body>
</html>

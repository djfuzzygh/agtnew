<!DOCTYPE html>
<html lang="fr">
    <head>

        <!-- Basic Page Needs
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <meta charset="utf-8">
        <title>AGT: Incroyable Talent</title>
        <meta name="description" content="L’Afrique A Un Incroyable Talent  a pour ambition de découvrir et mettre en valeur les meilleurs talents d’Afrique Francophone sous la supervision du jury composé de membres éminents de la scène culturelle Africaine : Angélique Kidjo, Fally Ipupa et Claudia Tagbo.
            Soutenu par son Partenaire Officiel Nescafé, ce programme séduit tous les téléspectateurs désireux de passer une belle soirée devant leur écran de télévision.
            Le Prix Nescafé donne l’opportunité au public de prendre la décision finale et de choisir leur candidat préféré en votant en ligne.">
        <meta name="author" content="">

        <!-- Mobile Specific Metas
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- FONT
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

        <!-- CSS
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <link rel="stylesheet" href="../css/normalize.css">
        <link rel="stylesheet" href="../css/skeleton.css">
        <link rel="stylesheet" href="../css/custom.css">
        <link rel="stylesheet" href="../css/sweetalert2.min.css">

        <!-- Scripts
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="../js/vote.js"></script>
        <script src="../js/sweetalert2.min.js"></script>


        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window,document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '302643959930499');
            fbq('track', 'PageView');
        </script>
        <noscript>
            <img height="1" width="1"
            src="https://www.facebook.com/tr?id=302643959930499&ev=PageView&noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->

        <!-- Favicon
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <link rel="icon" type="image/png" href="https://prod1.nescafe.com/cwa_en/public/img/favicon/favicon.ico">

    </head>
    <body>

        <!-- Primary Page Layout
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <div class="section header">
            <div class="row brand">
                <a href="/"><img src="../images/logo_03.png" alt="logo"></a>
            </div>
            <nav class="row navbar">
                <div class="container">
                  <a class="navbar-link" href="../index.php">#Accueil</a>
                  <a class="navbar-link" href="../finalistes.php">#Finalistes</a>
                  <a class="navbar-link" href="../reglement.php">#Réglement</a>
                  <!--a class="navbar-link" href="">#Profils</a>
                  <a class="navbar-link" href="#">#Gagnant</a-->
                </div>
            </nav>
        </div>
        <div class="section finalists">
          <!-- <div class="backgroundleft">
            <div class="backgroundright"> -->
            <div class="container">
                <div class="twelve columns profil">
                    <div class="row bio">
                        <img src="../images/profile/AANINKA.png" alt="" class="three columns"/>
                        <div class="eight columns">
                            <div class="profil-details">
                                <h5>Compagnie Aaninka</h5>
                                <h6>Danse | Côte D'Ivoire</h6>
                            </div>
                            <div class="five colums profil-description">
                                <p>Originaire de la Côte d’ivoire, la compagnie Aaninka danse sur des musiques Américaines mais avec des pas traditionnels, Africains. L’idée leur est venue à partir de la chanson Gangnam Style, avec laquelle ils ont commencé à  monter un spectacle traditionnel sur une musique urbaine. Unique en leur genre, ils véhiculent ainsi une ouverture d’esprit pleine de tradition et de modernité, un attachement aux racines tout en étant contemporain.</p>
                                <div>
                                  <p>La compagnie Aaninka voudrait montrer au monde qu’il ne faut pas se caser dans un seul coin, que les étiquettes non pas de sens et que les Africains peuvent danser sur toutes les musiques du monde.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row video-box">
                        <iframe width="853" height="480" src="https://www.youtube.com/embed/JmGM_8wUePM?rel=0" frameborder="0" allowfullscreen></iframe>
                        <button type="submit" class="vote-btn" id="aaninka">Votez pour ce talent<img src="../images/vote-icon.png"></button>
                        <h3 class="quote">"Ce que nous avons envie de montrer au monde entier, c’est la danse africaine!"</h3>
                    </div>
                    <div class="twelve columns foot-notes">
                        <div class="seven columns nescafe-logo">
                            <img src="../images/logo-white.png" alt="" class="">
                            <ul class="social">
                                <li><a href="https://www.facebook.com/Nescafe.GH/" target="_blank"><img src="../images/facebook.svg" alt=""></a></li>
                                <li><a href="https://twitter.com/NescafeAfrica" target="_blank"><img src="../images/twitter.svg" alt=""></a></li>
                                <li><a href="https://www.youtube.com/user/NESCAFEAfrica" target="_blank"><img src="../images/youtube.svg" alt=""></a></li>
                                <li><a href="https://www.instagram.com/nescafeafrica/" target="_blank"><img src="../images/instagram.svg" alt=""></a></li>
                            </ul>
                        </div>
                        <div class="four columns agt-logo">
                            <img src="../images/talent-logo.png" alt="" class="">
                            <ul class="row social agt-si">
                                <li><a href="https://www.facebook.com/afriqueaunincroyabletalent/" target="_blank"><img src="../images/facebook.svg" alt=""></a></li>
                                <li><a href="https://www.youtube.com/channel/UC0KCCM5Y1j74Lkkxp0VsHHw" target="_blank"><img src="../images/youtube.svg" alt=""></a></li>
                                <li><a href="https://www.instagram.com/incroyable_talent_afrique/" target="_blank"><img src="../images/instagram.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      <!-- </div>
    </div> -->

        <!-- End Document
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    </body>
</html>

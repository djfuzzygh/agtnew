$(document).ready(function(){
    //add facebook sdk
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '230234070766377',
            oauth      : true,
            xfbml      : true,
            status     : true,
            version    : 'v2.8'
        });
        FB.AppEvents.logPageView();


    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/fr_FR/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    // function check login status
    function checkLoginState() {
        FB.getLoginStatus(function(response) {
            statusChangeCallback(response);
        });
    }

    // This is called with the results from FB.getLoginStatus().
    function statusChangeCallback(response) {
        //console.log('statusChangeCallback');
        //console.log(response);
        // The response object is returned with a status field that lets the
        // app know the current login status of the person.
        // Full docs on the response object can be found in the documentation
        // for FB.getLoginStatus().
        if (response.status === 'connected') {
            // Logged into your app and Facebook.
            processClick();
        } else if (response.status === 'not_authorized') {
            // The person is logged into Facebook, but not your app.
            //document.getElementById('status').innerHTML = 'Please log ' +'into this app.';
            swal('Connectez-vous sur Facebook');
        } else {
            // The person is not logged into Facebook, so we're not sure if
            // they are logged into this app or not.
            swal('Connectez-vous sur Facebook');
        }
    }

    // Here we run a very simple test of the Graph API after login is
    // successful.  See statusChangeCallback() for when this call is made.
    function processClick() {
        console.log('Welcome!  Fetching your information.... ');

        FB.api('/me', function(response) {
            var user = response.id; //get FB UID
            console.log('Successful login for: ' + user);

            //get button id which is artist name
            var artist = $(".vote-btn").attr("id");

            //console.log('You voted ' + artist);

            //prepare post content
            post_data = {user:user, artist:artist, vote:"vote"};

            //send content to check_vote.php
            $.post("../scripts/check_vote.php", post_data, function(data){
                //replace vote count text with new values
                //$('.votes_counter').text(data);
                if (data === "success"){
                  swal("", "Merci d'avoir voté!","success");
                }
                else if (data === "voted"){
                  swal("", "you have already voted!","info");
                } else {
                  swal("Oops...","sorry, could not vote", "error");
                }

            }).fail(function(err) {
                //alert user about the HTTP server error
                //alert(err.statusText);
                swal("Oops...", err.statusText, "error");
            });
        });
    }

    function fb_login(){
        FB.login(function(){
            checkLoginState();
        },{scope: 'email, public_profile'});
    }

    //if yes, disable vote button
    /*document.getElementById('vote-btn').onclick = function () {
        this.disabled = true;
    }*/

    //button is clicked
    $(".vote-btn").click(function(e){
        //tie-in fbk-pixel event
        fbq('trackCustom', 'vote');

        //open facbook login dialogue to authenticate end user
        fb_login();

        //disable button
        //$(this).disabled = true;

    });

});
